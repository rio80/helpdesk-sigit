<?php
date_default_timezone_set("Asia/Jakarta");
require_once 'medoo.php';
    $database = new medoo([
        'database_type' => 'mysql',
        'database_name' => 'test_db',
        'server' => 'localhost',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8'
    ]);


// fungsi untuk menambah issue
function issuetambah($iduser,$namamu, $pecah)
{
    global $database;
    $last_id = $database->insert('bot', [
        'id'    => $iduser,
		'first_name' => $namamu,
		'tanggal'=>date('y-m-d'),
        'waktu' => date('H:i:s'),
        'pesan' =>$pecah[1],
		'pesan_dua'=>$pecah[2],
		'pesan_tiga'=>$pecah[3],
		'pesan_empat'=>$pecah[4],
		
    ]);
	
    return $last_id;
}

// fungsi untuk save user login ke DB
function simpanuser($iduser,$namamu)
{
	global $database;
	$last_id= $database->insert('login',[
	'id' => $iduser,
	'first_name'=>$namamu,
	'tanggal'=>date('y-m-d'),
	'waktu'=>date('H:i:s'),
	]);
	return $last_id;
}
 
// fungsi untuk ambil command (/) dari db
function ambilcommand($iduser,$katapertama)
{
	global $database;
	$last_id= $database->select('bot',[
	'id' => $iduser,
	'perintah'=>$katapertama,
	]);
	return $last_id;
}


// fungsi menghapus diary
function selesai($iduser, $idpesan)
{
    global $database;
    $database->delete('bot', [
        'AND' => [
            'id' => $iduser,
            'no' => $idpesan,
        ],
    ]);

    return '⛔️ telah dilaksanakan..';
}

// fungsi melihat daftar diary user
function diarylist($iduser, $page = 0)
{
    global $database;
    $hasil = '😢 Maaf, tidak ada list di dalam database kami';
    $datas = $database->select('bot', [
        'no',
        'id',
		'tanggal',
        'waktu',
        'pesan',
    ], [
        'id' => $iduser,
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $hasil = "Kami menemukan *$jml Issue*\n";
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            $hasil .= "\n$n. $data[pesan]\n  Jam       : `$data[waktu]`\n  Tanggal : `$data[tanggal]` \n";
            
        }
    }

    return $hasil;
}

// fungsi melihat daftar issue yang sudah selesai
function selesailist($iduser, $page = 0)
{
    global $database;
    $hasil = 'Maaf, belum ada issue yang telah diselesaikan';
    $datas = $database->select('audit_bot', [
        'no',
        'id',
		'tanggal_selesai',
        'pesan',
		'pesan_dua',
		'pesan_tiga',
		'pesan_empat',
		'no_issue',
    ], [
        'id' => $iduser,
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $hasil = "Terdapat *$jml Issue yang telah terselesaikan*\n";
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            $hasil .= "\n$n. Area \ Line : `*$data[pesan]*`\n  Masalah : `$data[pesan_dua]`\n Keternagan : `$data[pesan_tiga]`\n Shift : `$data[pesan_empat]`\n Tanggal Selesai : `$data[tanggal_selesai]` \n";
            
        }
    }

    return $hasil;
}

// fungsi melihat jadwal
function lihatjadwal($page = 0)
{
    global $database;
    $hasil = 'Maaf, belum ada issue yang telah diselesaikan';
    $datas = $database->select('calendar', [
        'id',
		'title',
        'startdate',
		'enddate',
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $hasil = "Terdapat *$jml Pekerjaan*\n";
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            $hasil .= "\n$n. $data[title]\n  Tanggal Mulai : `$data[startdate]`\n Tanggal Selesai : `$data[enddate]` \n";
            
        }
    }

    return $hasil;
}


// fungsi melihat semua issue
function lihatsemua($page = 0)
{
    global $database;
    $hasil = 'Maaf, Tidak ada Issue';
    $datas = $database->select('bot', [
        'pesan',
		'pesan_dua',
		'pesan_tiga',
		'tanggal',
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $hasil = " *$jml Issue yang telah terjadi*\n";
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            $hasil .= "\n$n. $data[pesan]\n  Masalah : `$data[pesan_dua]`\n Keterangan : `$data[pesan_tiga]`\n Tanggal Terjadi : `$data[tanggal]`\n";
            
        }
    }

    return $hasil;
}

function issueview($iduser, $idpesan)
{
    global $database;
    $hasil = "Issue Tidak Ditemukan";
    $datas = $database->select('bot', [
        'no',
        'id',
		'tanggal',
        'waktu',
        'pesan',
		'pesan_dua',
		'pesan_tiga',
		'pesan_empat',
    ], [
        'AND' => [
            'id' => $iduser,
            'no' => $idpesan,
        ],
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $data = $datas[0];
        $hasil = "✍🏽 Issue nomor $data[no] \n";
        $hasil .= "\nArea / Line : `$data[pesan]`\n Masalah : `$data[pesan_dua]`\n Keterangan : `$data[pesan_tiga]`\n Shift : `$data[pesan_empat]`\n\n⌛️ `$data[tanggal]` `$data[waktu]`";
        
    }
    return $hasil;
}



// fungsi mencari pesan di diary
function diarycari($iduser, $pesan)
{
    global $database;
    $hasil = 'Maaf data yang dicari tidak tersimpan dalam database kami.';
    $datas = $database->select('bot', [
        'no',
        'id',
        'waktu',
        'pesan',
    ], [
        'pesan[~]' => $pesan,
    ]);
    $jml = count($datas);
    if ($jml > 0) {
        $hasil = "✍ *$jml inventory selalu tersimpan dalam database kami\n";
        $n = 0;
        foreach ($datas as $data) {
            $n++;
            $hasil .= "\n$n. ".substr($data['pesan'], 0, 10)."...\n⌛️ `$data[waktu]`\n";
            $hasil .= "\n👀 /issue\_$data[no]\n";
        }
    }

    return $hasil;
}
