<?php
    require 'databasee.php';
    $id = 0;
     
    if ( !empty($_GET['no'])) {
        $id = $_REQUEST['no'];
    }
     
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['no'];
         
        // delete data
		$namaa="ANDHIKA";
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
		$sql = "DELETE FROM bot  WHERE no = ?";
		$q = $pdo->prepare($sql);
        $q->execute(array($id));
        Database::disconnect();
        header("Location: index.php");
         
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <link   href="css/bootstrap.min.css" rel="stylesheet">
    <script src="js/bootstrap.min.js"></script>
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>Hapus Data Defect </h3>
                    </div>
                     
                    <form class="form-horizontal" action="update.php" method="post">
                      <input type="hidden" name="no" value="<?php echo $id;?>"/>
                      <p class="alert alert-error">Yakin Menghapus ? ?</p>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-danger">Yes</button>
                          <a class="btn" href="index.php">No</a>
                        </div>
                    </form>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>