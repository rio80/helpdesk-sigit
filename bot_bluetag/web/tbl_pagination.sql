-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 02, 2015 at 10:00 AM
-- Server version: 5.5.41-cll-lve
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


-- --------------------------------------------------------

--
-- Table structure for table `tbl_pagination`
--

CREATE TABLE IF NOT EXISTS `tbl_pagination` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=72 ;

--
-- Dumping data for table `tbl_pagination`
--

INSERT INTO `tbl_pagination` (`id`, `first_name`) VALUES
(1, 'StepBlogging - 1'),
(2, 'StepBlogging - 2'),
(3, 'StepBlogging - 3'),
(4, 'StepBlogging - 4'),
(5, 'StepBlogging - 5'),
(6, 'StepBlogging - 6'),
(7, 'StepBlogging - 7'),
(8, 'StepBlogging - 8'),
(9, 'StepBlogging - 9'),
(10, 'StepBlogging - 10'),
(11, 'StepBlogging - 11'),
(12, 'StepBlogging - 12'),
(13, 'StepBlogging - 13'),
(14, 'StepBlogging - 14'),
(15, 'StepBlogging - 15'),
(16, 'StepBlogging - 16'),
(17, 'StepBlogging - 17'),
(18, 'StepBlogging - 18'),
(19, 'StepBlogging - 19'),
(20, 'StepBlogging - 20'),
(21, 'StepBlogging - 4'),
(22, 'StepBlogging - 5'),
(23, 'StepBlogging - 6'),
(24, 'StepBlogging - 7'),
(25, 'StepBlogging - 8'),
(26, 'StepBlogging - 9'),
(27, 'StepBlogging - 10'),
(28, 'StepBlogging - 11'),
(29, 'StepBlogging - 12'),
(30, 'StepBlogging - 13'),
(31, 'StepBlogging - 14'),
(32, 'StepBlogging - 15'),
(33, 'StepBlogging - 16'),
(34, 'StepBlogging - 17'),
(35, 'StepBlogging - 18'),
(36, 'StepBlogging - 19'),
(37, 'StepBlogging - 20'),
(38, 'StepBlogging - 4'),
(39, 'StepBlogging - 5'),
(40, 'StepBlogging - 6'),
(41, 'StepBlogging - 7'),
(42, 'StepBlogging - 8'),
(43, 'StepBlogging - 9'),
(44, 'StepBlogging - 10'),
(45, 'StepBlogging - 11'),
(46, 'StepBlogging - 12'),
(47, 'StepBlogging - 13'),
(48, 'StepBlogging - 14'),
(49, 'StepBlogging - 15'),
(50, 'StepBlogging - 16'),
(51, 'StepBlogging - 17'),
(52, 'StepBlogging - 18'),
(53, 'StepBlogging - 19'),
(54, 'StepBlogging - 20'),
(55, 'StepBlogging - 4'),
(56, 'StepBlogging - 5'),
(57, 'StepBlogging - 6'),
(58, 'StepBlogging - 7'),
(59, 'StepBlogging - 8'),
(60, 'StepBlogging - 9'),
(61, 'StepBlogging - 10'),
(62, 'StepBlogging - 11'),
(63, 'StepBlogging - 12'),
(64, 'StepBlogging - 13'),
(65, 'StepBlogging - 14'),
(66, 'StepBlogging - 15'),
(67, 'StepBlogging - 16'),
(68, 'StepBlogging - 17'),
(69, 'StepBlogging - 18'),
(70, 'StepBlogging - 19'),
(71, 'StepBlogging - 20');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
