-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2017 at 06:44 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `audit_bot`
--

CREATE TABLE IF NOT EXISTS `audit_bot` (
`no` int(50) NOT NULL,
  `id` int(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `pesan` varchar(255) NOT NULL,
  `pesan_dua` varchar(255) NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `pesan_empat` int(1) NOT NULL,
  `pesan_tiga` varchar(255) NOT NULL,
  `no_issue` int(4) NOT NULL,
  `nama_mechanic` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_bot`
--

INSERT INTO `audit_bot` (`no`, `id`, `first_name`, `pesan`, `pesan_dua`, `tanggal_selesai`, `pesan_empat`, `pesan_tiga`, `no_issue`, `nama_mechanic`) VALUES
(7, 236395409, 'Andhika', 'line P', 'mesin', '2016-11-07', 2, 'mesin macet', 229, ''),
(15, 236395409, 'Andhika', 'bbd12', 'mesin', '2016-11-11', 2, 'nyala', 210, 'Susanto'),
(21, 236395409, 'Andhika', 'RC13', 'material', '2016-11-11', 3, 'supply kurang', 230, 'Budi');

-- --------------------------------------------------------

--
-- Table structure for table `bot`
--

CREATE TABLE IF NOT EXISTS `bot` (
`no` int(50) NOT NULL,
  `id` int(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `waktu` time NOT NULL,
  `perintah` varchar(50) NOT NULL,
  `pesan` varchar(255) NOT NULL,
  `pesan_dua` varchar(255) NOT NULL,
  `pesan_tiga` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `pesan_empat` int(1) NOT NULL,
  `status` int(2) NOT NULL,
  `nama_mechanic` varchar(20) NOT NULL,
  `tanggal_planning` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `action` text NOT NULL,
  `progres` int(3) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bot`
--

INSERT INTO `bot` (`no`, `id`, `first_name`, `waktu`, `perintah`, `pesan`, `pesan_dua`, `pesan_tiga`, `tanggal`, `pesan_empat`, `status`, `nama_mechanic`, `tanggal_planning`, `tanggal_selesai`, `action`, `progres`) VALUES
(1, 0, 'Andhika', '00:00:00', '', 'Cell03', 'mesin', 'mesin panas', '2016-11-18', 2, 2, 'Budiman', '2016-11-22', '2016-11-23', 'tambah cooler', 100),
(2, 0, 'Andhika', '00:00:00', '', 'BBD13', 'material', 'kurang bahan', '2016-11-18', 1, 2, 'Tono', '2016-11-22', '2016-11-22', 'tambah', 100),
(4, 0, 'Andhika', '00:00:00', '', 'Modular4', 'mesin', 'mesin bermasalah', '2016-11-18', 2, 2, 'Tono', '2016-11-19', '2016-11-25', 'Perbaiki', 100),
(5, 0, 'Andhika', '00:00:00', '', 'BBD12', 'mesin', 'mesin mati', '2016-11-18', 1, 1, 'Budi', '2017-09-20', '0000-00-00', '', 50),
(6, 0, 'Andhika', '00:00:00', '', 'RC14', 'mesin', 'mold terbakar', '2016-11-23', 2, 1, 'Joko', '2016-11-25', '0000-00-00', '', 50),
(7, 0, 'Andhika', '00:00:00', '', 'Cell13', 'material', 'kurang material', '2016-11-23', 2, 1, 'Joko', '2016-11-23', '0000-00-00', '', 50),
(9, 0, 'bagor', '00:00:00', '', 'bagor', 'bagor', 'bagor', '2017-08-23', 2, 0, '', '0000-00-00', '0000-00-00', '', 0),
(13, 0, 'dd', '00:00:00', '', 'dd', 'dd', 'dd', '2017-08-25', 1, 0, '', '0000-00-00', '0000-00-00', '', 0),
(14, 0, 'ee', '09:34:04', '', 'ee', 'ee', 'ee', '2017-08-25', 2, 0, '', '0000-00-00', '0000-00-00', '', 0),
(15, 0, 'aa', '09:47:22', '', 'aa', 'aa', 'aa', '2017-08-25', 1, 0, '', '0000-00-00', '0000-00-00', '', 0),
(16, 0, 'wedus', '09:49:27', '', 'wedus', 'wedus', 'wedus', '2017-08-25', 1, 0, '', '0000-00-00', '0000-00-00', '', 0),
(17, 0, 'jaran', '09:49:39', '', 'jaran', 'jaran', 'jaran', '2017-08-25', 2, 0, '', '0000-00-00', '0000-00-00', '', 0),
(18, 0, 'aaa', '16:37:30', '', 'welding', 'Mesin', 'aa', '2017-09-05', 2, 0, '', '0000-00-00', '0000-00-00', '', 0),
(19, 0, 'jkjk', '22:26:42', '', 'hjhhj', 'klkl', 'ghgh', '2017-09-23', 1, 0, '', '0000-00-00', '0000-00-00', '', 0),
(20, 0, 'rio', '22:31:27', '', 'welding', 'mesin', 'kobong', '2017-09-23', 1, 0, '', '0000-00-00', '0000-00-00', '', 0);

--
-- Triggers `bot`
--
DELIMITER //
CREATE TRIGGER `input_calendar` BEFORE UPDATE ON `bot`
 FOR EACH ROW INSERT INTO calendar VALUES (id,old.pesan,NEW.tanggal_planning,NEW.tanggal_selesai,"false",old.nama_mechanic)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE IF NOT EXISTS `calendar` (
`id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `startdate` varchar(48) NOT NULL,
  `enddate` varchar(48) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  `nama_mechanic` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`id`, `title`, `startdate`, `enddate`, `allDay`, `nama_mechanic`) VALUES
(0, 'Cell13', '2016-11-23', '1970-01-01', 'false', 'Joko'),
(29, 'Cell03', '2016-11-22', '2016-11-23', 'false', 'Budiman'),
(30, 'Cell13', '2016-11-23', '0000-00-00', 'false', 'Joko');

-- --------------------------------------------------------

--
-- Table structure for table `data_issue_selesai`
--

CREATE TABLE IF NOT EXISTS `data_issue_selesai` (
`id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
  `id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `issue_selesai`
--

CREATE TABLE IF NOT EXISTS `issue_selesai` (
`id` int(5) NOT NULL,
  `no_issue` int(5) NOT NULL,
  `nama_mechanic` varchar(30) NOT NULL,
  `mesin` varchar(50) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `masalah` varchar(30) NOT NULL,
  `tanggal_planning` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issue_selesai`
--

INSERT INTO `issue_selesai` (`id`, `no_issue`, `nama_mechanic`, `mesin`, `keterangan`, `masalah`, `tanggal_planning`) VALUES
(55, 2, 'Tono', 'BBD13', 'material', 'kurang bahan', '2016-11-22'),
(61, 1, 'Budiman', 'Cell03', 'mesin', 'mesin panas', '2016-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `id` int(20) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `first_name`, `tanggal`, `waktu`) VALUES
(1, 'sigit', '2017-08-17', '00:00:21'),
(236395409, 'Andhika', '2016-11-09', '09:31:55');

-- --------------------------------------------------------

--
-- Table structure for table `login2`
--

CREATE TABLE IF NOT EXISTS `login2` (
`id` int(5) NOT NULL,
  `nrp` varchar(6) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `password` varchar(5) NOT NULL,
  `section` varchar(20) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login2`
--

INSERT INTO `login2` (`id`, `nrp`, `nama`, `password`, `section`, `level`) VALUES
(1, '09005', 'sigit', '09005', 'IT', 'admin'),
(2, '09006', 'benjo', '09006', 'welding', 'user'),
(3, '00005', 'candra', '00005', 'manager', 'manager'),
(4, '00005', 'candra', '00005', 'manager', 'manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `audit_bot`
--
ALTER TABLE `audit_bot`
 ADD PRIMARY KEY (`no`);

--
-- Indexes for table `bot`
--
ALTER TABLE `bot`
 ADD PRIMARY KEY (`no`), ADD KEY `no` (`no`);

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `data_issue_selesai`
--
ALTER TABLE `data_issue_selesai`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `issue_selesai`
--
ALTER TABLE `issue_selesai`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login2`
--
ALTER TABLE `login2`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `audit_bot`
--
ALTER TABLE `audit_bot`
MODIFY `no` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `bot`
--
ALTER TABLE `bot`
MODIFY `no` int(50) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `calendar`
--
ALTER TABLE `calendar`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `data_issue_selesai`
--
ALTER TABLE `data_issue_selesai`
MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `issue_selesai`
--
ALTER TABLE `issue_selesai`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `login2`
--
ALTER TABLE `login2`
MODIFY `id` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
