<?php
require_once '../../dbconfig.php';

require_once 'getLayout.html';
$db=$DB_con;
if(isset($_POST['btn-save']))
{ 
	if(isset($_GET['tipe'])=='edit'){
		
		try
		{
			$idmaintenance = $_POST['id_maintenance'];
			$namateknisi = $_POST['nama_teknisi'];
			$telp = $_POST['telp'];
			$spesialis = $_POST['spesialis'];
			
			$stmt = $db->prepare("UPDATE maintenance SET nama_teknisi=:namateknisi, telp=:telp, spesialis=:spesialis
				WHERE id_maintenance=:idmaintenance");
			$stmt->bindparam(":idmaintenance",$idmaintenance);
			$stmt->bindparam(":namateknisi",$namateknisi);
			$stmt->bindparam(":telp",$telp);
			$stmt->bindparam(":spesialis",$spesialis);


			$stmt->execute();
			header("Location: simpan.php?update");
			// header("Location: browse.php");
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			header("Location: simpan.php?failure");
		}
	}else{
		$namateknisi = $_POST['nama_teknisi'];
		$telp = $_POST['telp'];
		$spesialis = $_POST['spesialis'];

    // die($fname.'-'.$lname.'-'.$email.'-'.$contact.'-'.$tanggal);
		try
		{
			$stmt = $db->prepare("INSERT INTO maintenance(nama_teknisi,telp,spesialis) VALUES(:namateknisi, :telp, :spesialis)");
			$stmt->bindparam(":namateknisi",$namateknisi);
			$stmt->bindparam(":telp",$telp);
			$stmt->bindparam(":spesialis",$spesialis);

			$stmt->execute();
			header("Location: simpan.php?inserted");
			header("Location: browse.php");
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			header("Location: simpan.php?failure");
		}
	}
}

?>

<?php
if(isset($_GET['inserted']))
{
	?>
	<div class="container">
		<div class="alert alert-info">
			<strong>WOW!</strong> Record Maintenance was inserted successfully <a href="index.php">HOME</a>!
		</div>
	</div>
	<?php
}
else if(isset($_GET['failure']))
{
	?>
	<div class="container">
		<div class="alert alert-warning">
			<strong>SORRY!</strong> ERROR while inserting record Maintenance!
		</div>
	</div>
	<?php
}
else if(isset($_GET['update']))
{
	?>
	<div class="container">
		<div class="alert alert-info">
			<strong>WOW!</strong>  Record was updated successfully
		</div>
	</div>
	<?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

	<h2><th>Input dan Edit Master Maintenance</th></h2>
	<form method='post'>

		<table class='table table-bordered'>

			<?php
			$idmaintenance = '';
			$namateknisi = '';
			$telp = '';
			$spesialis = '';
			if(isset($_GET['tipe'])=='edit'){
				$db=$DB_con;
				$idmaintenance=$_GET['edit_id'];
				$query="SELECT * FROM maintenance WHERE id_maintenance='".$idmaintenance."'";
				$stmt = $db->prepare($query);
				$stmt->execute();

				$row=$stmt->fetch(PDO::FETCH_ASSOC);
				$idmaintenance = $row['id_maintenance'];
				$namateknisi = $row['nama_teknisi'];
				$telp = $row['telp'];
				$spesialis = $row['spesialis'];
			}

			?>
			<input type='hidden' name='id_maintenance' class='form-control' value="<?php echo $idmaintenance;?>" required>

			<tr>
				<td>Nama Teknisi</td>
				<td><input type='text' name='nama_teknisi' class='form-control' value="<?php echo $namateknisi;?>" required></td>
			</tr>

			<tr>
				<td>Telepon</td>
				<td>
					<input type='text' name='telp' value="<?php echo $telp;?>" class='form-control' required>
				</td>
			</tr>

			<tr>
				<td>Spesialis</td>
				<td>
					<input type='text' name='spesialis' value="<?php echo $spesialis;?>" class='form-control' required>
				</td>
			</tr>



			<tr>
				<td>

				</td>

				<td>
					<button type="submit" class="btn btn-primary" name="btn-save">
						<span class="glyphicon glyphicon-plus"></span> Simpan
					</button>  
					<a href="browse.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to Browse</a>

					<a href="../../index.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Kembali ke Menu</a><p>

				</td>
			</tr>

		</table>
	</form>


</div>

<?php include_once '../../footer.php'; ?>