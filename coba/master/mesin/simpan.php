<?php
require_once '../../dbconfig.php';

require_once 'getLayout.html';
$db=$DB_con;
if(isset($_POST['btn-save']))
{ 
	if(isset($_GET['tipe'])=='edit'){
		
		try
		{
			$idmesin = $_POST['id_mesin'];
			$namamesin = $_POST['nama_mesin'];
			$jenismesin = $_POST['jenis_mesin'];
			
			$stmt = $db->prepare("UPDATE mesin SET nama_mesin=:namamesin, jenis_mesin=:jenismesin where id_mesin=:idmesin");
			$stmt->bindparam(":idmesin",$idmesin);
			$stmt->bindparam(":namamesin",$namamesin);
			$stmt->bindparam(":jenismesin",$jenismesin);


			$stmt->execute();
			header("Location: simpan.php?update");
			// header("Location: browse.php");
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			header("Location: simpan.php?failure");
		}
	}else{
		$namamesin = $_POST['nama_mesin'];
		$jenismesin = $_POST['jenis_mesin'];

    // die($fname.'-'.$lname.'-'.$email.'-'.$contact.'-'.$tanggal);
		try
		{
			$stmt = $db->prepare("INSERT INTO mesin(nama_mesin,jenis_mesin) VALUES(:jenismesin, :namamesin)");
			$stmt->bindparam(":jenismesin",$namamesin);
			$stmt->bindparam(":namamesin",$jenismesin);

			$stmt->execute();
			header("Location: simpan.php?inserted");
			header("Location: browse.php");
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			header("Location: simpan.php?failure");
		}
	}
}

?>

<?php
if(isset($_GET['inserted']))
{
	?>
	<div class="container">
		<div class="alert alert-info">
			<strong>WOW!</strong> Record was inserted successfully <a href="index.php">HOME</a>!
		</div>
	</div>
	<?php
}
else if(isset($_GET['failure']))
{
	?>
	<div class="container">
		<div class="alert alert-warning">
			<strong>SORRY!</strong> ERROR while inserting record !
		</div>
	</div>
	<?php
}
else if(isset($_GET['update']))
{
	?>
	<div class="container">
		<div class="alert alert-info">
			<strong>WOW!</strong>  Record was updated successfully
		</div>
	</div>
	<?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

	<h2><th>Input dan Edit Master Mesin</th></h2>
	<form method='post'>

		<table class='table table-bordered'>

			<?php
			$idmesin='';
			$namamesin='';
			$jenismesin='';
			if(isset($_GET['tipe'])=='edit'){
				$db=$DB_con;
				$idmesin=$_GET['edit_id'];
				$query="select * from mesin where id_mesin='".$idmesin."'";
				$stmt = $db->prepare($query);
				$stmt->execute();

				$row=$stmt->fetch(PDO::FETCH_ASSOC);
				$idmesin=$row['id_mesin'];	
				$namamesin=$row['nama_mesin'];
				$jenismesin=$row['jenis_mesin'];
			}

			?>
			<input type='hidden' name='id_mesin' class='form-control' value="<?php echo $idmesin;?>" required>

			<tr>
				<td>Nama</td>
				<td><input type='text' name='nama_mesin' class='form-control' value="<?php echo $namamesin;?>" required></td>
			</tr>

			<tr>
				<td>Mesin</td>
				<td>
					<input type='text' name='jenis_mesin' value="<?php echo $jenismesin;?>" class='form-control' required>
				</td>
			</tr>



			<tr>
				<td>

				</td>

				<td>
					<button type="submit" class="btn btn-primary" name="btn-save">
						<span class="glyphicon glyphicon-plus"></span> Simpan
					</button>  
					<a href="browse.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to Browse</a>

					<a href="../../index.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Kembali ke Menu</a><p>

				</td>
			</tr>

		</table>
	</form>


</div>

<?php include_once '../../footer.php'; ?>