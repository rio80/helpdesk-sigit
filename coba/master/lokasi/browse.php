<?php
include_once '../../dbconfig.php';
?>
<?php include_once 'getLayout.html'; ?>



<div class="clearfix"></div>

<div class="container">
	<h2><th>Browse Data Lokasi</th></h2>
</div>

<div class="clearfix"></div><br />
<div class="container">
	<!-- 	<button style="margin-bottom:5px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span> Tambah Issue</button> -->

	<a href="simpan.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Records</a><p>
	<table class='table table-bordered table-responsive'>
		<tr bgcolor="blue" style="color:white">
			<th><center>No</th>
			<th><center>Lokasi</th>
			<th><center>PPIC</th>
			<th><center>Proses Data</th>
		</tr>
		<?php
		$db=$DB_con;
		$query="select * from plant order by id_plant";
		$stmt = $db->prepare($query);
		$stmt->execute();

		if($stmt->rowCount()>0)
		{
			$no=1;
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))

				{

					?>

					<tr>

						<td><center><?php print($row['id_plant']); ?></center></td>
						<td><?php print($row['lokasi']); ?></td>
						<td><?php print($row['ppic']); ?></td>



						<td align="center">
							
							<a href="simpan.php?edit_id=<?php print($row['id_plant']); ?>&tipe=edit" class="btn btn-info" role="button">Edit</a>

							<a href="delete.php?del_id=<?php print($row['id_plant']); ?>" class="btn btn-info" role="button">Delete</a>
						</td>


					</tr>
					<?php
				}
			}
			else
			{
				?>
				<tr>
					<td>Tidak Ada Data...</td>
				</tr>
				<?php
			}

			?>

		</table>

		<a href="../../index.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Kembali ke Menu</a><p>