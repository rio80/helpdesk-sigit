<?php
require_once '../../dbconfig.php';

require_once 'getLayout.html';
$db=$DB_con;
if(isset($_POST['btn-save']))
{ 
	if(isset($_GET['tipe'])=='edit'){
		
		try
		{
			$idplant = $_POST['id_plant'];
			$lokasi = $_POST['lokasi'];
			$ppic = $_POST['ppic'];
			
			$stmt = $db->prepare("UPDATE plant SET lokasi=:lokasi, ppic=:ppic where id_plant=:idplant");
			$stmt->bindparam(":idplant",$idplant);
			$stmt->bindparam(":lokasi",$lokasi);
			$stmt->bindparam(":ppic",$ppic);


			$stmt->execute();
			header("Location: simpan.php?update");
			// header("Location: browse.php");
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			header("Location: simpan.php?failure");
		}
	}else{

			$lokasi = $_POST['lokasi'];
			$ppic = $_POST['ppic'];

    // die($fname.'-'.$lname.'-'.$email.'-'.$contact.'-'.$tanggal);
		try
		{
			$stmt = $db->prepare("INSERT INTO plant(lokasi,ppic) VALUES(:lokasi, :ppic)");
			$stmt->bindparam(":lokasi",$lokasi);
			$stmt->bindparam(":ppic",$ppic);

			$stmt->execute();
			header("Location: simpan.php?inserted");
			header("Location: browse.php");
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			header("Location: simpan.php?failure");
		}
	}
}

?>

<?php
if(isset($_GET['inserted']))
{
	?>
	<div class="container">
		<div class="alert alert-info">
			<strong>WOW!</strong> Record Plant was inserted successfully <a href="index.php">HOME</a>!
		</div>
	</div>
	<?php
}
else if(isset($_GET['failure']))
{
	?>
	<div class="container">
		<div class="alert alert-warning">
			<strong>SORRY!</strong> ERROR while inserting record Plant!
		</div>
	</div>
	<?php
}
else if(isset($_GET['update']))
{
	?>
	<div class="container">
		<div class="alert alert-info">
			<strong>WOW!</strong>  Record Plant was updated successfully
		</div>
	</div>
	<?php
}
?>

<div class="clearfix"></div><br />

<div class="container">

	<h2><th>Input dan Edit Master Plant/Lokasi</th></h2>
	<form method='post'>

		<table class='table table-bordered'>

			<?php
			$idplant = '';
			$lokasi = '';
			$ppic = '';
			
			if(isset($_GET['tipe'])=='edit'){
				$idplant=$_GET['edit_id'];
				$query="select * from plant where id_plant='".$idplant."'";
				$stmt = $db->prepare($query);
				$stmt->execute();

				$row=$stmt->fetch(PDO::FETCH_ASSOC);
				$idplant=$row['id_plant'];	
				$lokasi=$row['lokasi'];
				$ppic=$row['ppic'];
			}

			?>
			<input type='hidden' name='id_plant' class='form-control' value="<?php echo $idplant;?>" required>

			<tr>
				<td>Nama</td>
				<td><input type='text' name='lokasi' class='form-control' value="<?php echo $lokasi;?>" required></td>
			</tr>

			<tr>
				<td>Mesin</td>
				<td>
					<input type='text' name='ppic' value="<?php echo $ppic;?>" class='form-control' required>
				</td>
			</tr>



			<tr>
				<td>

				</td>

				<td>
					<button type="submit" class="btn btn-primary" name="btn-save">
						<span class="glyphicon glyphicon-plus"></span> Simpan
					</button>  
					<a href="browse.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to Browse</a>

					<a href="../../index.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Kembali ke Menu</a><p>

				</td>
			</tr>

		</table>
	</form>


</div>

<?php include_once '../../footer.php'; ?>