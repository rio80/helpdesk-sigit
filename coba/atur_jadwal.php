<?php
include_once 'dbconfig.php';
if(isset($_POST['btn-update']))
{
	$id = $_GET['edit_id'];
	$id = $_POST['no'];
	$fmesin = $_POST['pesan'];
	$fmasalah = $_POST['pesan_dua'];
	$fketerangan = $_POST['pesan_tiga'];
	$fmechanic = $_POST['nama_mechanic'];
	$fplanning = $_POST['tanggal_planning'];
	$fselesai = $_POST['tanggal_selesai'];
	
	
	if($crud->planning($id,$fmesin,$fmasalah,$fketerangan,$fmechanic,$fplanning,$fselesai))
	{
		$msg = "<div class='alert alert-info'>
				<strong>SUKSES!</strong> ISSUE akan selesaikan oleh:  $fmechanic. Planing: $fplanning & Duedate : $fselesai, <a href='index.php'>Kembali ke Home</a>
				</div>";
	}
	else
	{
		$msg = "<div class='alert alert-warning'>
				<strong>SORRY!</strong> Terjadi kesalahan !
				</div>";
	}
}

if(isset($_GET['edit_id']))
{
	$id = $_GET['edit_id'];
	extract($crud->getID($id));	
}

?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
	echo $msg;
}
?>
</div>

<div class="clearfix"></div><br />

<div class="container">
	 
     <form method='post'>
 
    <table class='table table-bordered'>
 
        <tr>
            <td>NO ISSUE</td>
            <td><input readonly   name='no' class='form-control' value="<?php echo $id; ?>" required></td>
        </tr>
 
		<tr>
            <td>Mesin / Area</td>
            <td><input readonly  name='pesan' class='form-control' value="<?php echo $pesan; ?>" required></td>
        </tr>
 
        <tr>
            <td>Masalah</td>
            <td><input readonly  name='pesan_dua' class='form-control' value="<?php echo $pesan_dua; ?>" required></td>
        </tr>
 
        <tr>
            <td>Keterangan</td>
            <td><input readonly  name='pesan_tiga' class='form-control' value="<?php echo $pesan_tiga; ?>" required></td>
        </tr>
 
        <tr>
            <td>Shift</td>
            <td><input readonly  name='pesan_empat' class='form-control'  value="<?php echo $pesan_empat; ?>" required></td>
        </tr>
		
		  <tr>
            <td>Mechanic</td>
            <td><input type='text' readonly name='nama_mechanic' class='form-control' value="<?php echo $nama_mechanic;?>"  required></td>
        </tr>
		
		
		<tr>
		<td>Planing Date</td>
        <td><input class="form-control" id="date" name="tanggal_planning" placeholder="MM/DD/YYY" type="text"/></td>
		</tr>
		
		<tr>
		<td>Due Date</td>
        <td><input class="form-control" id="date" name="tanggal_selesai" placeholder="MM/DD/YYY" type="text"/></td>
		</tr>
		
        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
    			<span class="glyphicon glyphicon-edit"></span>  YES
				</button>
                <a href="index.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </td>
        </tr>
 
    </table>
</form>
     </div>

	 <!-----------SCRIPT UNTUK DATETIMEPICKER--------------------------------------------------->
	 <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker ------------>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        var date_input=$('input[name="tanggal_planning"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'mm/dd/yyyy',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>


<?php include_once 'footer.php'; ?>