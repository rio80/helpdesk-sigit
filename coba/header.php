<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>WARNING SIGN</title>


	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen"> 
	<script type="text/javascript" src="./../coba/bootstrap/js/jquery.min.js"></script>
	<script type="text/javascript" src="./../coba/bootstrap/js/bootstrap.js"></script>
	<link href='assets/css/fullcalendar.css' rel='stylesheet' />
	<link href='assets/css/fullcalendar.print.css' rel='stylesheet' media='print' />
	<script src="./../coba/bootstrap/js/rangeSlider.js"></script>
	<script src='assets/js/moment.min.js'></script>
	<script src='assets/js/jquery.min.js'></script>
	<script src='assets/js/jquery-ui.min.js'></script>
	<script src='assets/js/fullcalendar.min.js'></script>


	

	<script>

		$(document).ready(function() {

		var zone = "05:30";  //Change this to your timezone

		$.ajax({
			url: 'process.php',
        type: 'POST', // Send post data
        data: 'type=fetch',
        async: false,
        success: function(s){
        	json_events = s;
        }
    });


		var currentMousePos = {
			x: -1,
			y: -1
		};
		jQuery(document).on("mousemove", function (event) {
			currentMousePos.x = event.pageX;
			currentMousePos.y = event.pageY;
		});

		/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});


		/* initialize the calendar
		-----------------------------------------------------------------*/

		$('#calendar').fullCalendar({
			events: JSON.parse(json_events),
			//events: [{"id":"14","title":"New Event","start":"2015-01-24T16:00:00+04:00","allDay":false}],
			utc: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			editable: true,
			droppable: true, 
			slotDuration: '00:30:00',
			eventReceive: function(event){
				var title = event.title;
				var start = event.start.format("YYYY-MM-DD[T]HH:mm:SS");
				$.ajax({
					url: 'process.php',
					data: 'type=new&title='+title+'&startdate='+start+'&zone='+zone,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						event.id = response.eventid;
						$('#calendar').fullCalendar('updateEvent',event);
					},
					error: function(e){
						console.log(e.responseText);

					}
				});
				$('#calendar').fullCalendar('updateEvent',event);
				console.log(event);
			},
			eventDrop: function(event, delta, revertFunc) {
				var title = event.title;
				var start = event.start.format();
				var end = (event.end == null) ? start : event.end.format();
				$.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
							revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
			},
			eventClick: function(event, jsEvent, view) {
				console.log(event.id);
				var title = prompt('Event Title:', event.title, { buttons: { Ok: true, Cancel: false} });
				if (title){
					event.title = title;
					console.log('type=changetitle&title='+title+'&eventid='+event.id);
					$.ajax({
						url: 'process.php',
						data: 'type=changetitle&title='+title+'&eventid='+event.id,
						type: 'POST',
						dataType: 'json',
						success: function(response){	
							if(response.status == 'success')			    			
								$('#calendar').fullCalendar('updateEvent',event);
						},
						error: function(e){
							alert('Error processing your request: '+e.responseText);
						}
					});
				}
			},
			eventResize: function(event, delta, revertFunc) {
				console.log(event);
				var title = event.title;
				var end = event.end.format();
				var start = event.start.format();
				$.ajax({
					url: 'process.php',
					data: 'type=resetdate&title='+title+'&start='+start+'&end='+end+'&eventid='+event.id,
					type: 'POST',
					dataType: 'json',
					success: function(response){
						if(response.status != 'success')		    				
							revertFunc();
					},
					error: function(e){		    			
						revertFunc();
						alert('Error processing your request: '+e.responseText);
					}
				});
			},
			eventDragStop: function (event, jsEvent, ui, view) {
				if (isElemOverDiv()) {
					var con = confirm('Are you sure to delete this event permanently?');
					if(con == true) {
						$.ajax({
							url: 'process.php',
							data: 'type=remove&eventid='+event.id,
							type: 'POST',
							dataType: 'json',
							success: function(response){
								console.log(response);
								if(response.status == 'success'){
									$('#calendar').fullCalendar('removeEvents');
									getFreshEvents();
								}
							},
							error: function(e){	
								alert('Error processing your request: '+e.responseText);
							}
						});
					}   
				}
			}
		});

		function getFreshEvents(){
			$.ajax({
				url: 'process.php',
	        type: 'POST', // Send post data
	        data: 'type=fetch',
	        async: false,
	        success: function(s){
	        	freshevents = s;
	        }
	    });
			$('#calendar').fullCalendar('addEventSource', JSON.parse(freshevents));
		}


		function isElemOverDiv() {
			var trashEl = jQuery('#trash');

			var ofs = trashEl.offset();

			var x1 = ofs.left;
			var x2 = ofs.left + trashEl.outerWidth(true);
			var y1 = ofs.top;
			var y2 = ofs.top + trashEl.outerHeight(true);

			if (currentMousePos.x >= x1 && currentMousePos.x <= x2 &&
				currentMousePos.y >= y1 && currentMousePos.y <= y2) {
				return true;
		}
		return false;
	}

});

</script>
<style>

	

	#trash{
		width:32px;
		height:32px;
		float:left;
		padding-bottom: 15px;
		position: relative;
	}

	#wrap {
		width: 1100px;
		margin: 0 auto;
	}

	#external-events {
		float: left;
		width: 150px;
		padding: 0 10px;
		border: 1px solid #ccc;
		background: #eee;
		text-align: left;
	}

	#external-events h4 {
		font-size: 16px;
		margin-top: 0;
		padding-top: 1em;
	}

	#external-events .fc-event {
		margin: 10px 0;
		cursor: pointer;
	}

	#external-events p {
		margin: 1.5em 0;
		font-size: 11px;
		color: #666;
	}

	#external-events p input {
		margin: 0;
		vertical-align: middle;
	}

	#calendar {
		float: right;
		width: 900px;
	}

</style>
</head>
<body>
	<?php
	$con = mysqli_connect('localhost','root','','tes_db');
	$sql="SELECT COUNT(*) FROM bot";
	$result = mysqli_query($con,$sql);
	$row = mysqli_fetch_array($result);
	$sql2 = "SELECT COUNT(*) FROM bot WHERE status='1'";
	$result2= mysqli_query($con,$sql2);
	$row2=mysqli_fetch_array($result2);
	?>
	<div class="navbar navbar-default navbar-static-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">

				<a class="navbar-brand" href="index.php">
					<img src="kmi.jpg" class="img-circle" alt="Cinque Terre" width="110" height="110" ></a>
				</div>

				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Issue  <span class="badge"><?php echo $row['COUNT(*)'] ?></span>
							<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="index.php">Board Issue</a></li>
								<li class="divider"></li>
								<li><a href="selesai.php">Issue Selesai</a></li>
								<li class="divider"></li>
								<li><a href="data_plan.php">Data Plan <span class="badge"><?php echo $row2['COUNT(*)'] ?></span></a></li>
								<li class="divider"></li>
							</ul>
						</li>
					</ul>

					<ul class="nav navbar-nav">
						<li class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" style="cursor:pointer">Jadwal
								<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="calendar.php">Jadwal</a></li>
									<li class="divider"></li>
										<!--<li><a href="list_jadwal.php">List Jadwal</a></li>-->
										<li><a href="todo.php">Todo List</a></li>
									</ul>
								</li>
							</ul>

							<ul class="nav navbar-nav">
								<li class="dropdown">
									<a class="dropdown-toggle" data-toggle="dropdown" href="index.php">Master Input
										<span class="caret"></span></a>
										<ul class="dropdown-menu">
											<li><a href="master/lokasi/browse.php">Plant/Area</a></li>
											<li class="divider"></li>
											<li><a href="master/Maintenance/browse.php">Maintenance</a></li>
											<li class="divider"></li>
											<li><a href="master/mesin/browse.php">Mesin</a></li>

										</ul>
									</li>
								</ul>

								<ul class="nav navbar-nav navbar-right">
									<li><a data-toggle="modal" data-target="#modalinfo"><span class='glyphicon glyphicon-comment'></span> Info</a></li>
								</ul>
							</div>
        <!--    <a class="navbar-brand" href="./../bot_bluetag/index.php" title='Display'>Display</a>
        <a class="navbar-brand" href="selesai.php" title='Display'>Selesai</a>  -->
    </div>

    <div id="modalinfo" class="modal fade">
    	<div class="modal-dialog">
    		<div class="modal-content">
    			<div class="modal-header">
    				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    				<h4 class="modal-tittle">INFO</h4>
    			</div>
    			<div class="modal-body">
    				<?php
    				$sql2 = "SELECT COUNT(*) FROM bot WHERE status='1'";
    				$result2= mysqli_query($con,$sql2);
    				$row2=mysqli_fetch_array($result2);
    				$sql="SELECT COUNT(*) FROM bot";
    				$result = mysqli_query($con,$sql);
    				$row = mysqli_fetch_array($result);
    				$sql3 = "SELECT COUNT(*) FROM bot WHERE status='2'";
    				$result3= mysqli_query($con,$sql3);
    				$row3=mysqli_fetch_array($result3);
    				echo "<div style='padding:5px' class='alert alert-warning'><span class='glyphicon glyphicon-info-sign'></span>Terdapat  : <a style='color:red'>".$row['COUNT(*)']."</a> Issue yang belum terselesaikan</div>";
    				echo "<div style='padding:5px' class='alert alert-warning'><span class='glyphicon glyphicon-info-sign'></span>Terdapat  : <a style='color:red'>".$row2['COUNT(*)']."</a> Issue yang baru di planning</div>"; 
    				echo "<div style='padding:5px' class='alert alert-warning'><span class='glyphicon glyphicon-info-sign'></span>Terdapat  : <a style='color:red'>".$row3['COUNT(*)']."</a> Issue yang sudah terselesaikan</div>";


    				?>
    			</div>
    		</div>
    	</div>
    </div>