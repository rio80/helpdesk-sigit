<?php
include_once 'dbconfig.php';
include_once 'config.php';
if(isset($_POST['btn-update']))
{
	$id = $_GET['edit_id'];
	$fnama = $_POST['first_name'];
	$fmesin = $_POST['pesan'];
	$fmasalah = $_POST['pesan_dua'];
	$fketerangan = $_POST['pesan_tiga'];
	$fshift = $_POST['pesan_empat'];
	$fmechanic = $_POST['nama_mechanic'];
	$fstatus = $_POST['status'];
	$fprogres = $_POST['progres'];
	/*$date12 =mysqli_real_escape_string ($con,$_POST['tanggal_planning']);
	$fplanning = date('YYYY-mm-dd',strtotime($date12));*/
	$date12 =mysqli_real_escape_string ($con,$_POST['tanggal_planning']);
	$fplanning = date('Y-m-d',strtotime($date12));
	// die($fplanning);
	if($crud->update($id,$fnama,$fmesin,$fmasalah,$fketerangan,$fshift,$fmechanic,$fplanning,$fstatus,$fprogres))
	{
		$msg = "<div class='alert alert-info'>
				<strong>SUKSES!</strong> ISSUE akan dikerjakan oleh $fmechanic , <a href='index.php'>Kembali ke HOME</a>
				</div>";
	}
	else
	{
		$msg = "<div class='alert alert-warning'>
				<strong>SORRY!</strong> ERROR while updating record !
				</div>";
	}
}

if(isset($_GET['edit_id']))
{
	$id = $_GET['edit_id'];
	extract($crud->getID($id));	
}

?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
	echo $msg;
}
?>
</div>

<div class="clearfix"></div><br />

<div class="container">
	 
     <form method='post'>
 
    <table class='table table-bordered'>
 

        <tr>
            <td>Nama</td>
            <td><input readonly   name='first_name' class='form-control' value="<?php echo $first_name; ?>" required></td>
			<td><input readonly type="hidden" name='status' class='form-control' value="1"</td>
			<td><input readonly type="hidden" name='progres' class='form-control' value="50"</td>
        </tr>
 
		<tr>
            <td>Section</td>
            <td><input readonly  name='pesan' class='form-control' value="<?php echo $pesan; ?>" required></td>
        </tr>
 
        <tr>
            <td>Incharge</td>
            <td><input readonly  name='pesan_dua' class='form-control' value="<?php echo $pesan_dua; ?>" required></td>
        </tr>
 
        <tr>
            <td>Description</td>
            <td><input readonly  name='pesan_tiga' class='form-control' value="<?php echo $pesan_tiga; ?>" required></td>
        </tr>
 
        <tr>
            <td>Shift</td>
            <td><input readonly  name='pesan_empat' class='form-control'  value="<?php echo $pesan_empat; ?>" required></td>
        </tr>
		
		  <tr>
            <td>Teknisi</td>
            <td><input type='text' placeholder="Isi dengan nama Mechanic" name='nama_mechanic' class='form-control' value="<?php echo $nama_mechanic;?>"  required></td>
        </tr>
		
	
		<tr>
		<td>Planning Date</td>
		<td><input type='text'  name='tanggal_planning' id='tanggal_planning' class='form-control' value="<?php echo $tanggal_planning;?>" placeholder="YYYY-mm-dd"  required></td>
        
		</tr>
		
		
        <tr>
            <td colspan="2">
                <button type="submit" class="btn btn-primary" name="btn-update">
    			<span class="glyphicon glyphicon-edit"></span>  YES
				</button>
                <a href="index.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; CANCEL</a>
            </td>
        </tr>
 
    </table>
</form>
     </div>

	
	
	 <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<!-- Include Date Range Picker ------------>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<script>
    $(document).ready(function(){
        var date_input=$('input[name="tanggal_planning"]'); //our date input has the name "date"
        var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
        date_input.datepicker({
            format: 'YYYY-mm-dd',
            container: container,
            todayHighlight: true,
            autoclose: true,
        })
    })
</script>


<?php include_once 'footer.php'; ?>