<?php
include_once 'dbconfig.php';
if(isset($_POST['btn-save']))
{
	$fname = $_POST['first_name'];
	$lname = $_POST['mesin'];
	$email = $_POST['lokasi'];
	$contact = $_POST['pesan_tiga'];
    $shift = $_POST['pesan_empat'];
	$tanggal = date('Y-m-d');
	
    // die($fname.'-'.$lname.'-'.$email.'-'.$contact.'-'.$tanggal);
	if($crud->create($fname,$lname,$email,$contact,$shift,$tanggal))
	{
		header("Location: add-data.php?inserted");
	}
	else
	{
		header("Location: add-data.php?failure");
	}
}
?>
<?php include_once 'header.php'; ?>
<div class="clearfix"></div>

<?php
if(isset($_GET['inserted']))
{
	?>
    <div class="container">
     <div class="alert alert-info">
        <strong>WOW!</strong> Record was inserted successfully <a href="index.php">HOME</a>!
    </div>
</div>
<?php
}
else if(isset($_GET['failure']))
{
	?>
    <div class="container">
     <div class="alert alert-warning">
        <strong>SORRY!</strong> ERROR while inserting record !
    </div>
</div>
<?php
}
?>

<div class="clearfix"></div><br/>

<div class="container">
<h2><th>Input HelpDesk</th></h2>

  <form method='post'>

    <table class='table table-bordered'>

        <tr>
            <td>Nama</td>
            <td><input type='text' name='first_name' class='form-control' required></td>
        </tr>

        <tr>
            <td>Mesin</td>
            <td>
                <select name='mesin' class='form-control'>
                <option value=''></option>
                    <?php
                    $db = $DB_con;
                    $query='select nama_mesin from mesin';
                    $stmt = $db->prepare($query);
                    $stmt->execute();
                    if($stmt->rowCount()>0){
                       while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                        echo "<option value='".$row['nama_mesin']."'>"
                        .$row['nama_mesin']."</option>";
                    }
                }
                ?>
            </select>
           <!--  <input type='text' name='pesan' class='form-control' required> -->
        </td>
    </tr>

    <tr>
        <td>Section</td>
        <td>
        <select name='lokasi' class='form-control'>
        <option value=''></option>
                    <?php
                    $db = $DB_con;
                    $query='select lokasi from plant';
                    $stmt = $db->prepare($query);
                    $stmt->execute();
                    if($stmt->rowCount()>0){
                       while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
                        echo "<option value='".$row['lokasi']."'>"
                        .$row['lokasi']."</option>";
                    }
                }
                ?>
            </select>
        </td>
    </tr>

    <tr>
        <td>Masalah</td>
        <td><input type='text' name='pesan_tiga' class='form-control' required></td>
    </tr>
    <tr>
        <td>Shift</td>
        <td><input type='number' min=0 max=2 name='pesan_empat' class='form-control'  required></td>
    </tr>

    <tr>
        <td colspan="2">
            <button type="submit" class="btn btn-primary" name="btn-save">
              <span class="glyphicon glyphicon-plus"></span> Data Baru
          </button>  
          <a href="index.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to index</a>
      </td>
  </tr>

</table>
</form>


</div>

<?php include_once 'footer.php'; ?>