


<?php
include_once 'dbconfig.php';
?>
<?php include_once 'header.php'; ?>



<div class="clearfix"></div><br />
<div class="container">
	<h2><th>WARNING LAMP</th></h2>
<!-- 	<button style="margin-bottom:5px" data-toggle="modal" data-target="#myModal" class="btn btn-info col-md-2"><span class="glyphicon glyphicon-plus"></span> Tambah Issue</button> -->

<a href="add-data.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Records</a><p>                
		<div class="table-responsive">
			<table id="data-pasien" class="table table-striped table-bordered">


		<tr>
			<th>No</th>
			<th><center>No Tiket</center></th>
			<th><center>Nama</center></th>
			<th><center>Tanggal</center></th>
			<th><center>Jam</center></th>
			<th><center>Section</center></th>     
			<th><center>Incharge</center></th>     
			<th><center>Deskripsi</center></th>
			<th><center>Shift</center></th>
			<th><center>Lama Tunggu</center></th>
			<th><center>Status</center></th>
			<th colspan="1" align="center"><center>Actions</center></th>
			<th><center>Progres</center></th>
		</tr>
		<?php
		$query = "SELECT * FROM bot";       
		$records_per_page=10;
		$newquery = $crud->paging($query,$records_per_page);
		$crud->dataview($newquery);
		?>
		<tr>
			<td colspan="7" align="center">
				<div class="pagination-wrap">
					<?php $crud->paginglink($query,$records_per_page); ?>
				</div>
			</td>
		</tr>

	</table>
	</div>
</div>
<?php include_once 'footer.php'; ?>

<div id="myModal" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-tittle">Masukan Issue</h4>
			</div>
			<div class="modal-body">
				<form action="tmb_issue_act.php" method="post">
					<div class="form-group">
						<label>Nama</label>
						<input name="first_name" type="text" class="form-control" placeholder="Masukan Nama Pelapor . . " required>
					</div>
					<div class="form-group">
						<label>Area / Mesin</label>
						<input name="pesan" type="text" class="form-control" placeholder="Masukan Area / Mesin / Line . . " required>
					</div>
					<div class="form-group">
						<label>Tipe Masalah</label>
						<input name="pesan_dua" type="text" class="form-control" placeholder="Masukan Kategori Masalah . ." required>
					</div>
					<div class="form-group">
						<label>Issue</label>
						<input name="pesan_tiga" type="text" class="form-control" placeholder="Masukan Issue . ." required>
					</div>
					<div class="form-group">
						<label>Shift</label>
						<input name="pesan_empat" type="text" class="form-control" placeholder="Masukan Shift . ." required>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
				<input type="submit" class="btn btn-primary" value="Simpan">
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('#data-pasien').DataTable();
	});
</script>

