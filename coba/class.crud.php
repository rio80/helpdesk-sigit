<?php

class crud
{
	private $db;
	
	function __construct($DB_con)
	{
		$this->db = $DB_con;
	}
	
	public function create($fname,$lname,$email,$contact,$shift,$tanggal)
	{
		try
		{
			$stmt = $this->db->prepare("INSERT INTO bot(first_name,pesan,pesan_dua,pesan_tiga,pesan_empat,tanggal) VALUES(:fname, :lname, :email, :contact,:shift,:tanggal)");
			$stmt->bindparam(":fname",$fname);
			$stmt->bindparam(":lname",$lname);
			$stmt->bindparam(":email",$email);
			$stmt->bindparam(":contact",$contact);
			$stmt->bindparam(":shift",$shift);
			$stmt->bindparam(":tanggal",$tanggal);
			$stmt->execute();
			return true;
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
		
	}
	
	public function getID($id)
	{
		$stmt = $this->db->prepare("SELECT * FROM bot WHERE no=:no");
		$stmt->execute(array(":no"=>$id));
		$editRow=$stmt->fetch(PDO::FETCH_ASSOC);
		return $editRow;
	}
	
	public function update($id,$fnama,$fmesin,$fmasalah,$fketerangan,$fshift,$fmechanic,$fplanning,$fstatus,$fprogres)
	{
		try
		{
			$stmt=$this->db->prepare("UPDATE bot SET first_name=:fnama, 
		                                               pesan=:fmesin, 
													   pesan_dua=:fmasalah, 
													   pesan_tiga=:fketerangan,
													   pesan_empat=:fshift,
													   nama_mechanic=:fmechanic,
													   tanggal_planning=:fplanning,
													   status=:fstatus,
													   progres=:fprogres
													   WHERE no=:no ");
			$stmt->bindparam(":fnama",$fnama);
			$stmt->bindparam(":fmesin",$fmesin);
			$stmt->bindparam(":fmasalah",$fmasalah);
			$stmt->bindparam(":fketerangan",$fketerangan);
			$stmt->bindparam(":fshift",$fshift);
			$stmt->bindparam(":fmechanic",$fmechanic);
			$stmt->bindparam(":fplanning",$fplanning);
			$stmt->bindparam(":fstatus",$fstatus);
			$stmt->bindparam(":fprogres",$fprogres);
			$stmt->bindparam(":no",$id);
			$stmt->execute();
			
			return true;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
	}
	
	public function updateselesai($id,$fnama,$fmesin,$fmasalah,$fketerangan,$fshift,$fmechanic,$fplanning,$fselesai,$faction,$fstatus,$fprogres)
	{
		try
		{
			$stmt=$this->db->prepare("UPDATE bot SET first_name=:fnama, 
		                                               pesan=:fmesin, 
													   pesan_dua=:fmasalah, 
													   pesan_tiga=:fketerangan,
													   pesan_empat=:fshift,
													   nama_mechanic=:fmechanic,
													   tanggal_planning=:fplanning,
													   tanggal_selesai = :fselesai,
													   action = :faction,
													   status=:fstatus,
													   progres=:fprogres
													   WHERE no=:no ");
			$stmt->bindparam(":fnama",$fnama);
			$stmt->bindparam(":fmesin",$fmesin);
			$stmt->bindparam(":fmasalah",$fmasalah);
			$stmt->bindparam(":fketerangan",$fketerangan);
			$stmt->bindparam(":fshift",$fshift);
			$stmt->bindparam(":fmechanic",$fmechanic);
			$stmt->bindparam(":fplanning",$fplanning);
			$stmt->bindparam(":fselesai",$fselesai);
			$stmt->bindparam(":faction",$faction);
			$stmt->bindparam(":fstatus",$fstatus);
			$stmt->bindparam(":fprogres",$fprogres);
			$stmt->bindparam(":no",$id);
			$stmt->execute();
			
			return true;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
	}
	
	public function planning($id,$fmesin,$fmasalah,$fketerangan,$fmechanic,$fplanning,$fselesai)
	{
		try
		{
			$stmt=$this->db->prepare("UPDATE bot SET 
		                                               pesan=:fmesin, 
													   pesan_dua=:fmasalah, 
													   pesan_tiga=:fketerangan,
													   nama_mechanic=:fmechanic,
													   tanggal_planning=:fplanning,
													   tanngal_selesai=:fselesai
													   WHERE no=:no ");
			$stmt->bindparam(":fmesin",$fmesin);
			$stmt->bindparam(":fmasalah",$fmasalah);
			$stmt->bindparam(":fketerangan",$fketerangan);
			$stmt->bindparam(":fmechanic",$fmechanic);
			$stmt->bindparam(":fplanning",$fplanning);
			$stmt->bindparam(":fselesai",$fselesai);
			$stmt->bindparam(":no",$id);
			$stmt->execute();
			
			return true;	
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();	
			return false;
		}
	}
	
	
	public function delete($id)
	{
		$stmt = $this->db->prepare("DELETE FROM bot WHERE no=:no");
		$stmt->bindparam(":no",$id);
		$stmt->execute();
		return true;
	}
	
	/* paging */
	
	public function dataview($query)
	
	{
		$stmt = $this->db->prepare($query);
		$stmt->execute();
		
		if($stmt->rowCount()>0)
		{
			$no=1;
			while($row=$stmt->fetch(PDO::FETCH_ASSOC))
				
				{
					date_default_timezone_set("Asia/Jakarta");
					$time1 =strtotime($row['waktu']);
					$time2 = time();
					$selisih2 =($time2-$time1);
					$selisih = floor($selisih2/60);
					
					?>
					
					<tr>
						<td><?php print $no++;?></td>
						<td style='color:red'><center><?php print($row['no']); ?></center></td>
						<td><center><?php print($row['first_name']); ?></center></td>
						<td><?php print($row['tanggal']); ?></td>
						<td><?php print($row['waktu']); ?></td>
						<td><?php print($row['pesan']); ?></td>				
						<td><?php print($row['pesan_dua']); ?></td>
						<td><?php print($row['pesan_tiga']); ?></td>
						<td><center><?php print($row['pesan_empat']); ?></td>
						<td><center><?php print($selisih);?></center></td>
						<td><center><?php if($row['status']=='0'){echo "Submitted";}elseif($row['status']=='1') {echo "Planned";}else{echo "Done";}?></center>
							
							<td align="center">
								<!-- <a href="delete.php?delete_id=<?php print($row['no']); ?>"><i  class="glyphicon glyphicon-remove-circle"></i></a>		-->
								<a href="edit-data.php?edit_id=<?php print($row['no']); ?>"class="btn btn-info" <?php if ($row['status'] == '2'){ ?> disabled <?php   } ?> role="button">Plan</a>
							</td>
							<td> <div class="progress-bar 
								progress-bar-success 
								progress-bar-striped" 
								role="progressbar" 
								aria-valuenow="0"
								aria-valuemin="0" 
								aria-valuemax="100" 
								style="width:
								<?php echo $row['progres']; ?>%;">
								<?php echo $row['progres'];?> %
							</div>
						</td>
					</tr>
					<?php
				}
			}
			else
			{
				?>
				<tr>
					<td>Tidak Ada Data...</td>
				</tr>
				<?php
			}
			
		}
		
		
		public function dataplan($query)
		
		{
			$stmt = $this->db->prepare($query);
			$stmt->execute();
			
			if($stmt->rowCount()>0)
			{
				while($row=$stmt->fetch(PDO::FETCH_ASSOC))
					{
						
						date_default_timezone_set("Asia/Jakarta");
						$time1 =strtotime($row['waktu']);
						$time2 = time();
						$selisih2 =($time2-$time1);
						$selisih = floor($selisih2/60);
						
						?>
						<tr>
							
							<td><center><?php print($row['no']); ?></center></td>
							<td><center><?php print($row['first_name']); ?></center></td>
							<td><?php print($row['tanggal']); ?></td>
							<td><?php print($row['waktu']); ?></td>
							<td><?php print($row['pesan']); ?></td>				
							<td><?php print($row['pesan_dua']); ?></td>
							<td><?php print($row['pesan_tiga']); ?></td>
							<td><center><?php print($row['pesan_empat']); ?></td>
							<td><center><?php if($row['status']=='0'){echo "Submitted";}elseif($row['status']=='1') {echo "Planned";}else{echo "Done";}?></center>
								
								<td align="center">
									<!-- <a href="delete.php?delete_id=<?php print($row['no']); ?>"><i  class="glyphicon glyphicon-remove-circle"></i></a>		-->
									<a href="data_selesai.php?edit_id=<?php print($row['no']); ?>"class="btn btn-info" role="button">Plan</a>
								</td>
							</tr>
							<?php
						}
					}
					else
					{
						?>
						<tr>
							<td>Nothing here...</td>
						</tr>
						<?php
					}
					
				}
				
				public function dataselesai($query)
				
				{
					$stmt = $this->db->prepare($query);
					$stmt->execute();
					
					if($stmt->rowCount()>0)
					{
						while($row=$stmt->fetch(PDO::FETCH_ASSOC))
							{
								
								date_default_timezone_set("Asia/Jakarta");
								$time1 =strtotime($row['waktu']);
								$time2 = time();
								$selisih2 =($time2-$time1);
								$selisih = floor($selisih2/60);
								
								?>
								<tr>
									
									<td><center><?php print($row['no']); ?></center></td>
									<td><center><?php print($row['first_name']); ?></center></td>
									<td><?php print($row['pesan']); ?></td>				
									<td><?php print($row['pesan_dua']); ?></td>
									<td><?php print($row['pesan_tiga']); ?></td>
									<td><center><?php print($row['pesan_empat']); ?></td>
									<td><?php print($row['tanggal_planning']);?></td>
									<td><?php print($row['tanggal_selesai']);?></td>
									<td><?php print($row['nama_mechanic']);?></td>
									<td><?php print($row['action']);?></td>
									<td><center><?php if($row['status']=='0'){echo "Submitted";}elseif($row['status']=='1') {echo "Planned";}else{echo '<button class="btn btn-success">Done ';}?></center>
										
										
									</tr>
									<?php
								}
							}
							else
							{
								?>
								<tr>
									<td>Nothing here...</td>
								</tr>
								<?php
							}
							
						}
						
						public function dataviewaudit($query)
						{
							$stmt = $this->db->prepare($query);
							$stmt->execute();
							
							if($stmt->rowCount()>0)
							{
								while($row=$stmt->fetch(PDO::FETCH_ASSOC))
									{
										?>
										<tr>
											<td><?php print($row['id']); ?></td>
											<td><?php print($row['no_issue']); ?></td>
											<td><?php print($row['nama_mechanic']); ?></td>
											<td><?php print($row['mesin']); ?></td>
											<td><?php print($row['masalah']); ?></td>
											<td><?php print($row['keterangan']); ?></td>				
											<td><?php print($row['tanggal_planning']); ?></td>
											<td><a class="btn btn-warning" data-toggle="modal" data-target="#myModal">Planned</a>
												<script>$('#myModal').modal('show')</script>
												<td align="center">
													<a href="#"><i class="glyphicon glyphicon-edit"></i></a>
												</td>
											</tr>
											<?php
										}
									}
									else
									{
										?>
										<tr>
											<td>Nothing here...</td>
										</tr>
										<?php
									}
									
								}
								
								
								public function listjadwal($query)
								{
									$stmt = $this->db->prepare($query);
									$stmt->execute();
									
									if($stmt->rowCount()>0)
									{
										while($row=$stmt->fetch(PDO::FETCH_ASSOC))
											{
												?>
												<tr>
													<td><?php print($row['id']); ?></td>
													<td><?php print($row['title']); ?></td>
													<td><?php print($row['startdate']); ?></td>
													<td><?php print($row['enddate']); ?></td>            
			<!--	<td align="center">
			<a href="edit-data.php?edit_id=  <?php print($row['id']); ?>"><i class="glyphicon glyphicon-edit"></i></a>-->
		</td>
	</tr>
	<?php
}
}
else
{
	?>
	<tr>
		<td>Nothing here...</td>
	</tr>
	<?php
}

}




/*-----------------PAGING---------------------*/


public function paging($query,$records_per_page)
{
	$starting_position=0;
	if(isset($_GET["page_no"]))
	{
		$starting_position=($_GET["page_no"]-1)*$records_per_page;
	}
	$query2=$query." limit $starting_position,$records_per_page";
	return $query2;
}

public function paginglink($query,$records_per_page)
{
	
	$self = $_SERVER['PHP_SELF'];
	
	$stmt = $this->db->prepare($query);
	$stmt->execute();
	
	$total_no_of_records = $stmt->rowCount();
	
	if($total_no_of_records > 0)
	{
		?><ul class="pagination"><?php
		$total_no_of_pages=ceil($total_no_of_records/$records_per_page);
		$current_page=1;
		if(isset($_GET["page_no"]))
		{
			$current_page=$_GET["page_no"];
		}
		if($current_page!=1)
		{
			$previous =$current_page-1;
			echo "<li><a href='".$self."?page_no=1'>First</a></li>";
			echo "<li><a href='".$self."?page_no=".$previous."'>Previous</a></li>";
		}
		for($i=1;$i<=$total_no_of_pages;$i++)
		{
			if($i==$current_page)
			{
				echo "<li><a href='".$self."?page_no=".$i."' style='color:red;'>".$i."</a></li>";
			}
			else
			{
				echo "<li><a href='".$self."?page_no=".$i."'>".$i."</a></li>";
			}
		}
		if($current_page!=$total_no_of_pages)
		{
			$next=$current_page+1;
			echo "<li><a href='".$self."?page_no=".$next."'>Next</a></li>";
			echo "<li><a href='".$self."?page_no=".$total_no_of_pages."'>Last</a></li>";
		}
		?></ul><?php
	}
}

/* paging */

}
