<?php include_once 'header.php'?>

<?php require 'connect.php'; ?>

<body>
<div class="wrap">
		<div class="task-list">
		<h2 style='color:red'> Todo List</h2>
			<ul>
				<?php
				require ('connect.php');
				$query = mysqli_query($connect, "SELECT * FROM bot WHERE status='1' ");
				$numrows = mysqli_num_rows($query);

				if ($numrows > 0) {
					while ($row = mysqli_fetch_assoc($query)) {
						$task_id = $row['no'];
						$task_name = $row['pesan'];
						$task_issue = $row['pesan_tiga'];
						$task_mechanic = $row['nama_mechanic'];
						$task_planning =$row['tanggal_planning'];
					 echo '<li>
					 <span><b>Nama Mechanic</b> :<b> '.$task_mechanic.' </b></span>
                    <span><b>Issue</b> :'.$task_name.' , '.$task_issue.'</span>
					<span><b>Planning Date</b>: '.$task_planning.'</span>
					<img id="'.$task_id.'" class="delete-button" width="10px" src="images/close.svg" />
    				</li>';
					}
				}
				?>
			</ul>
		</div>

</div>
</body>

<!-- JavaScript/Jquery-->
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<script>

	add_task();
	delete_task();

function add_task() {
	$('.add-new-task').submit(function(){

		var new_task = $('.add-new-task input[name=new-task]').val();

		if (new_task !='') {
			$.post('add-task.php', {task: new_task},
				function(data) {
					$('.add-new-task input[name=new-task]').val('');
					$(data).appendTo('.task-list ul').hide().fadeIn();
					delete_task();
				});
		} return false;
	});
}

function delete_task(){

	$('.delete-button').click(function(){

		var current_element = $(this);

		var id = $(this).attr('no');

		$.post('delete-task.php', { task_id: id }, function() {

					current_element.parent().fadeOut("fast", function() { $(this).remove(); });
		});
	});
}
</script>

</html>

<?php include_once 'footer.php'; ?>