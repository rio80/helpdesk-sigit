<?php
include_once 'dbconfig.php';
if(isset($_POST['btn-update']))
{
	$id = $_GET['edit_id'];
	$fnama = $_POST['first_name'];
	$fmesin = $_POST['pesan'];
	$fmasalah = $_POST['pesan_dua'];
	$fketerangan = $_POST['pesan_tiga'];
	$fshift = $_POST['pesan_empat'];
	$fmechanic = $_POST['nama_mechanic'];
	$fplanning = $_POST['tanggal_planning'];
	
	
	if($crud->update($id,$fnama,$fmesin,$fmasalah,$fketerangan,$fshift,$fmechanic,$fplanning))
	{
		$msg = "<div class='alert alert-info'>
				<strong>SUKSES!</strong> ISSUE akan dikerjakan oleh $fmechanic , <a href='index.php'>Kembali ke HOME</a>
				</div>";
	}
	else
	{
		$msg = "<div class='alert alert-warning'>
				<strong>SORRY!</strong> ERROR while updating record !
				</div>";
	}


if(isset($_GET['edit_id']))
{
	$id = $_GET['edit_id'];
	extract($crud->getID($id));	
}

?>
<?php include_once 'header.php'; ?>

<div class="clearfix"></div>

<div class="container">
<?php
if(isset($msg))
{
	echo $msg;
}
?>
</div>

<div class="modal-dialog">
    <div class="modal-content">

    	<div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h4 class="modal-title" id="myModalLabel">Edit Data Menggunakan Modal Boostrap (popup)</h4>
        </div>

        <div class="modal-body">
        	<form action="proses_edit.php" name="modal_popup" enctype="multipart/form-data" method="POST">
        		
                <div class="form-group" style="padding-bottom: 20px;">
                	<label for="Modal Name">Modal Name</label>
                    <input type="hidden" name="modal_id"  class="form-control" value="<?php echo $r['no']; ?>" />
     				<input type="text" name="modal_name"  class="form-control" value="<?php echo $r['first_name']; ?>"/>
                </div>

                <div class="form-group" style="padding-bottom: 20px;">
                	<label for="Description">Description</label>
     				<textarea name="description"  class="form-control"><?php echo $r['pesan']; ?></textarea>
                </div>

                <div class="form-group" style="padding-bottom: 20px;">
                	<label for="Date">Date</label>       
     				<input type="text" name="date"  class="form-control" value="<?php echo $r['date']; ?>" disabled/>
                </div>

	            <div class="modal-footer">
	                <button class="btn btn-success"  name="btn-update" type="submit">
	                    Confirm
	                </button>

	                <button type="reset" class="btn btn-danger"  data-dismiss="modal" aria-hidden="true">
	               		Cancel
	                </button>
	            </div>

            	</form>

             <?php } ?>

            </div>

           
        </div>
    </div>