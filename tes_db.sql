﻿# Host: localhost  (Version 5.5.5-10.1.25-MariaDB)
# Date: 2017-12-06 21:59:34
# Generator: MySQL-Front 5.4  (Build 4.112) - http://www.mysqlfront.de/

/*!40101 SET NAMES utf8 */;

#
# Structure for table "audit_bot"
#

DROP TABLE IF EXISTS `audit_bot`;
CREATE TABLE `audit_bot` (
  `no` int(50) NOT NULL AUTO_INCREMENT,
  `id` int(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `pesan` varchar(255) NOT NULL,
  `pesan_dua` varchar(255) NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `pesan_empat` int(1) NOT NULL,
  `pesan_tiga` varchar(255) NOT NULL,
  `no_issue` int(4) NOT NULL,
  `nama_mechanic` varchar(50) NOT NULL,
  PRIMARY KEY (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

#
# Data for table "audit_bot"
#

INSERT INTO `audit_bot` VALUES (7,236395409,'Andhika','line P','mesin','2016-11-07',2,'mesin macet',229,''),(15,236395409,'Andhika','bbd12','mesin','2016-11-11',2,'nyala',210,'Susanto'),(21,236395409,'Andhika','RC13','material','2016-11-11',3,'supply kurang',230,'Budi');

#
# Structure for table "bot"
#

DROP TABLE IF EXISTS `bot`;
CREATE TABLE `bot` (
  `no` int(50) NOT NULL AUTO_INCREMENT,
  `id` int(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `waktu` time NOT NULL,
  `perintah` varchar(50) NOT NULL,
  `pesan` varchar(255) NOT NULL,
  `pesan_dua` varchar(255) NOT NULL,
  `pesan_tiga` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `pesan_empat` int(1) NOT NULL,
  `status` int(2) NOT NULL,
  `nama_mechanic` varchar(20) NOT NULL,
  `tanggal_planning` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `action` text NOT NULL,
  `progres` int(3) NOT NULL,
  PRIMARY KEY (`no`),
  KEY `no` (`no`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

#
# Data for table "bot"
#

INSERT INTO `bot` VALUES (1,0,'Andhika','00:00:00','','Cell03','mesin','mesin panas','2016-11-18',2,2,'Budiman','2016-11-22','2016-11-23','tambah cooler',100),(2,0,'Andhika','00:00:00','','BBD13','material','kurang bahan','2016-11-18',1,2,'Tono','2016-11-22','2016-11-22','tambah',100),(4,0,'Andhika','00:00:00','','Modular4','mesin','mesin bermasalah','2016-11-18',2,2,'Tono','2016-11-19','2016-11-25','Perbaiki',100),(5,0,'Andhika','00:00:00','','BBD12','mesin','mesin mati','2016-11-18',1,1,'Budi','0000-00-00','0000-00-00','',50),(6,0,'Andhika','00:00:00','','RC14','mesin','mold terbakar','2016-11-23',2,1,'Joko','2016-11-25','0000-00-00','',50),(7,0,'Andhika','00:00:00','','Cell13','material','kurang material','2016-11-23',2,1,'Joko','2016-11-23','0000-00-00','',50),(9,0,'bagor','00:00:00','','bagor','bagor','bagor','2017-08-23',2,0,'','0000-00-00','0000-00-00','',0),(13,0,'dd','00:00:00','','dd','dd','dd','2017-08-25',0,2,'Rudi','2017-12-12','2017-12-13','bersihkan',100),(14,0,'ee','09:34:04','','ee','ee','ee','2017-08-25',2,0,'','0000-00-00','0000-00-00','',0),(15,0,'aa','09:47:22','','aa','aa','aa','2017-08-25',1,0,'','0000-00-00','0000-00-00','',0),(16,0,'wedus','09:49:27','','wedus','wedus','wedus','2017-08-25',1,0,'','0000-00-00','0000-00-00','',0),(17,0,'jaran','09:49:39','','jaran','jaran','jaran','2017-08-25',2,0,'','0000-00-00','0000-00-00','',0),(18,0,'aaa','16:37:30','','welding','Mesin','aa','2017-09-05',2,0,'','0000-00-00','0000-00-00','',0),(19,0,'jkjk','22:26:42','','hjhhj','klkl','ghgh','2017-09-23',1,0,'','0000-00-00','0000-00-00','',0),(20,0,'rio','22:31:27','','welding','mesin','kobong','2017-09-23',1,1,'caca','2017-12-27','0000-00-00','',50),(21,0,'Rio F','17:18:24','','Bending Roll','scrap','banyak scrap','2017-11-29',2,1,'Budi','2017-12-05','0000-00-00','',50),(22,0,'Rio F','23:54:32','','Bending Roll','skrap','banyak scrap','2017-11-30',2,2,'Budi','0000-00-00','1970-01-01','bersihkan',100),(23,0,'tes 5','00:00:00','','aaA','scSS','VVVV','2017-12-06',0,2,'JESSICA','2017-12-29','2017-12-30','bersihkan',100),(24,0,'Jonathan','00:00:00','','Bending Roll Medium','P2','2','2017-12-06',0,0,'','0000-00-00','0000-00-00','',0),(25,0,'Nudin','00:00:00','','Bending Roll Medium','P2','Kebakaran','2017-12-06',0,0,'','0000-00-00','0000-00-00','',0),(26,0,'Budi Gunawab','00:00:00','','Bending Roll Long','P3','Skraappppppppppp','2017-12-06',2,0,'','0000-00-00','0000-00-00','',0);

#
# Structure for table "calendar"
#

DROP TABLE IF EXISTS `calendar`;
CREATE TABLE `calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `startdate` varchar(48) NOT NULL,
  `enddate` varchar(48) NOT NULL,
  `allDay` varchar(5) NOT NULL,
  `nama_mechanic` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

#
# Data for table "calendar"
#

INSERT INTO `calendar` VALUES (0,'Cell13','2016-11-23','1970-01-01','false','Joko'),(29,'Cell03','2016-11-22','2016-11-23','false','Budiman'),(30,'Cell13','2016-11-23','0000-00-00','false','Joko');

#
# Structure for table "data_issue_selesai"
#

DROP TABLE IF EXISTS `data_issue_selesai`;
CREATE TABLE `data_issue_selesai` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "data_issue_selesai"
#


#
# Structure for table "image"
#

DROP TABLE IF EXISTS `image`;
CREATE TABLE `image` (
  `id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` blob NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "image"
#


#
# Structure for table "issue_selesai"
#

DROP TABLE IF EXISTS `issue_selesai`;
CREATE TABLE `issue_selesai` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `no_issue` int(5) NOT NULL,
  `nama_mechanic` varchar(30) NOT NULL,
  `mesin` varchar(50) NOT NULL,
  `keterangan` varchar(30) NOT NULL,
  `masalah` varchar(30) NOT NULL,
  `tanggal_planning` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

#
# Data for table "issue_selesai"
#

INSERT INTO `issue_selesai` VALUES (55,2,'Tono','BBD13','material','kurang bahan','2016-11-22'),(61,1,'Budiman','Cell03','mesin','mesin panas','2016-11-22');

#
# Structure for table "login"
#

DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(20) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `tanggal` date NOT NULL,
  `waktu` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Data for table "login"
#

INSERT INTO `login` VALUES (1,'sigit','2017-08-17','00:00:21'),(236395409,'Andhika','2016-11-09','09:31:55');

#
# Structure for table "login2"
#

DROP TABLE IF EXISTS `login2`;
CREATE TABLE `login2` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nrp` varchar(6) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `password` varchar(5) NOT NULL,
  `section` varchar(20) NOT NULL,
  `level` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "login2"
#

INSERT INTO `login2` VALUES (1,'09005','sigit','09005','IT','admin'),(2,'09006','benjo','09006','welding','user'),(3,'00005','candra','00005','manager','manager'),(4,'00005','candra','00005','manager','manager');

#
# Structure for table "maintenance"
#

DROP TABLE IF EXISTS `maintenance`;
CREATE TABLE `maintenance` (
  `id_maintenance` int(11) NOT NULL AUTO_INCREMENT,
  `nama_teknisi` varchar(100) NOT NULL,
  `telp` varchar(15) NOT NULL,
  `spesialis` varchar(30) NOT NULL,
  PRIMARY KEY (`id_maintenance`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

#
# Data for table "maintenance"
#

INSERT INTO `maintenance` VALUES (1,'Budi Gunawan','08568877662','Gerinda'),(2,'Asep Saefudin','08187662232','Las karbit'),(3,'Junaedi aaaaa','08898232323333','Cat Besi Mengkilap'),(4,'Wawan kuncoro','0818232323','Forklift');

#
# Structure for table "mesin"
#

DROP TABLE IF EXISTS `mesin`;
CREATE TABLE `mesin` (
  `id_mesin` int(11) NOT NULL AUTO_INCREMENT,
  `nama_mesin` varchar(40) NOT NULL,
  `jenis_mesin` varchar(20) NOT NULL,
  PRIMARY KEY (`id_mesin`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

#
# Data for table "mesin"
#

INSERT INTO `mesin` VALUES (2,'Bending Roll Medium','B03'),(3,'Bending Roll Medium slow','B099 cccc'),(4,'Bending Roll Long','B06'),(5,'CNC 21','CNC 21 B222'),(6,'Milling 333','Milling 333 b44'),(7,'Painting BLASH','P1432');

#
# Structure for table "plant"
#

DROP TABLE IF EXISTS `plant`;
CREATE TABLE `plant` (
  `id_plant` int(11) NOT NULL AUTO_INCREMENT,
  `lokasi` varchar(20) NOT NULL,
  `ppic` varchar(20) NOT NULL,
  PRIMARY KEY (`id_plant`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

#
# Data for table "plant"
#

INSERT INTO `plant` VALUES (1,'P1','Wawan Gunawan'),(2,'P2','Ridho Roma'),(3,'P333','Muhammad Udin Salahu');
